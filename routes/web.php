<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'checkSession' ], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/user', 'UserController@index');
    Route::get('/list', 'UserController@list');
    Route::get('/add',  'UserController@tambah');
    Route::get('/edit/{id}',  'UserController@edit');
    Route::get('/delete/{id}',  'UserController@delete');

    //route mahasiswa
    Route::get('/mahasiswa', 'MahasiswaController@index');
    Route::get('/list-mahasiswa', 'MahasiswaController@listmahasiswa');
    Route::get('/add-mahasiswa', 'MahasiswaController@create');
    Route::post('/simpan-mahasiswa', 'MahasiswaController@addmahasiswa');
    Route::get('/delete-mahasiswa/{nim}',  'MahasiswaController@delete');
    Route::get('/edit-mahasiswa/{nim}', 'MahasiswaController@edit');
    Route::post('/update-mahasiswa', 'MahasiswaController@update');

    //route daftar
    Route::get('/daftar', 'DaftarController@index');
    Route::get('/list-daftar','DaftarController@listdaftar');
    Route::get('/add-daftar','DaftarController@create');
    Route::post('simpan-daftar', 'DaftarController@adddaftar');
    Route::get('/delete-daftar/{iddaftar}','DaftarController@delete');
    Route::get('/edit-daftar/{iddaftar}', 'DaftarController@edit');
    Route::post('/update-daftar','DaftarController@update');
    
    Route::get('/', 'HomeController@index');

    Route::post('/simpan', 'UserController@simpan');
    Route::post('/update', 'UserController@update');
});

Route::get('logout', function ()
{
    auth()->logout();
    Session()->flush();

    return Redirect::to('/masuk');
})->name('logout');

route::get('/login','HomeController@login');
Route::get('/masuk', 'HomeController@masuk');
