<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Daftar;
use App\Mahasiswa;
class DaftarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.daftar');
    }
    Public function listdaftar(){
        $data = daftar::select('iddaftar','tgldaftar','semester','tahun', 'nim', 'pendapatan', 'ipk', 'saudara')->get();
        $tabel ['draw']                 = '1';
        $tabel ['recordsTotal']         =  count($data);
        $tabel ['recordsFiltered']  =  count($data);
        $tabel ['data']                 = $data;
        return json_encode($tabel) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $mahasiswa= Mahasiswa::All();
        return view('master.adddaftar', compact('mahasiswa'));
    }
    public function delete($iddaftar){
       
       Daftar::where('iddaftar', $iddaftar)->delete();
        return redirect('/daftar');
    }
    public function adddaftar(Request $request){
     
       
        // $daftar->iddaftar = $request->iddaftar;
        // $daftar->tgldaftar = $request->tgldaftar;
        // $daftar->semester = $request->semester;
        // $daftar->tahun = $request->tahun;
        // $daftar->nim =$request->nim;
        // $daftar->pendapatan = $request->pendapatan;
        // $daftar->ipk = $request->ipk;
        // $daftar->saudara =$request->saudara;
        // $daftar->save();
        Daftar::create($request->all());
  
        return view('master.daftar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $mahasiswa= Mahasiswa::All();
        
        $iddaftar = $request->iddaftar;
        $data = Daftar::where('iddaftar',$iddaftar)->first();
        return view('master.daftaredit',compact('mahasiswa'),['daftar'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $iddaftar=$request->iddaftar;
        Daftar::where('iddaftar',$iddaftar)->update([
            'iddaftar' => $request->iddaftar,
            'tgldaftar' => $request->tgldaftar,
            'semester' => $request->semester,
            'tahun'=>$request->tahun,
            'nim'=>$request->nim,
            'pendapatan'=>$request->pendapatan,
            'ipk'=>$request->ipk,
            'saudara'=>$request->saudara
        ]);
        return redirect('/daftar');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
