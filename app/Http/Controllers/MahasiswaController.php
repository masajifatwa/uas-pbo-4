<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Mahasiswa;
use App\sysmenu;
class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = sysmenu::where('sysmenu_id','=','0')
        ->with('childrenCategories')
        ->get();
        return view('master.mahasiswa',['data_menu'=>$categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('master.addmahasiswa');
    }
    public function addmahasiswa(Request $request)
    {
       Mahasiswa::create($request->all());
       return view('master.mahasiswa');

    }
    public function delete($nim){
        // Mahasiswa::where($nim)->delete();
       Mahasiswa::where('nim', $nim)->delete();
        return redirect('/mahasiswa');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function listmahasiswa(Request $request){
        $data = Mahasiswa::select('nim','nama','alamat','telp')->get();
        $tabel ['draw']                 = '1';
        $tabel ['recordsTotal']         =  count($data);
        $tabel ['recordsFiltered']  =  count($data);
        $tabel ['data']                 = $data;
        return json_encode($tabel) ;
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $nim = $request->nim;
        $data = Mahasiswa::where('nim',$nim)->first();
        return view('master.mahasiswaedit',['mahasiswa'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $nim=$request->nim;
        Mahasiswa::where('nim',$nim)->update([
            'nim' => $request->nim,
            'nama' => $request->nama,
            'alamat' => $request->alamat,
            'telp'=>$request->telp
        ]);
        return redirect('/mahasiswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
