@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Data Mahasiswa</h3>
            </div>
            <div class="card-body">
                <form role="form" method="POST" action="{{asset('')}}simpan-mahasiswa">
			{{ csrf_field() }}
                   <div class="card-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label>NIM</label>
                                <input type="text" class="form-control" name="nim" placeholder="Masukan NIM">
                            </div>
                            <label>Nama Mahasiswa</label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Lengkap">
                        </div>
                        
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="alamat" placeholder="Masukan Alamat">
                        </div>
                        <div class="form-group">
                            <label>Telp</label>
                            <input type="text" class="form-control" name="telp" placeholder="No Telepon">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection