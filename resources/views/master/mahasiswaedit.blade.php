@extends('layout.app')

@section('style')

@endsection

@section('script')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Data User</h3>
            </div>
            <div class="card-body">
                <form role="form" method="POST" action="{{asset('')}}update-mahasiswa">
                   @csrf 
                    <div class="card-body">
                        <div class="form-group">
                            <label>NIM</label>
                            {{-- <input type="hidden" class="form-control" name="txtid" value="{{$mahasiswa->id}}"> --}}
                            <input type="text"  class="form-control" name="nim" value="{{$mahasiswa->nim}}" placeholder="Nama Lengkap">
                        </div>
                        <div class="form-group">
                            <label>Nama Mahasiswa</label>
                            <input type="text" class="form-control" name="nama" value="{{$mahasiswa->nama}}" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <input type="text" class="form-control" name="alamat" value="{{$mahasiswa->alamat}}" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label>telp</label>
                            <input type="text" class="form-control" name="telp" value="{{$mahasiswa->telp}}" placeholder="Password">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection