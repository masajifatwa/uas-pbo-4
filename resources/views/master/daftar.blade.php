@extends('layout.app')

@section('style')
<link rel="stylesheet" href="{{asset ('')}}plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{asset ('')}}plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
@endsection

@section('script')
<!-- DataTables -->
<script src="{{asset('')}}plugins/datatables/jquery.dataTables.min.js"></script>
<script src="{{asset('')}}plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('')}}plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="{{asset('')}}plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script type="text/javascript">
   $(function () {
    var table = $('#example2').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{ asset('') }}list-daftar",
        columns: [
            { data: 'iddaftar', name: 'iddaftar' },
            {data: 'tgldaftar', name: 'tgldaftar'},
            {data: 'semester', name: 'semester'},
            {data: 'tahun', name: 'tahun'},
            {data: 'nim', name: 'nim'},
            {data: 'pendapatan', name: 'pendapatan'},
            {data: 'ipk', name: 'ipk'},
            {data: 'saudara', name: 'saudara'},
   
            {
             data: null,
             className: "center",
             defaultContent: '<a href="#" class="edit btn btn-primary btn-sm"><i class="far fa-edit"></i></a> <a href="#" class="delete btn btn-danger btn-sm"><i class="fas fa-trash"></i></a>' 
            }
        ]
    });

    $('#example2 tbody').on( 'click', 'a.edit', function () {
        var data = table.row( $(this).parents('tr') ).data();
        var txt = '{{asset('')}}edit-daftar/'+data.iddaftar;
        location = txt;
    } );

    $('#example2 tbody').on( 'click', 'a.delete', function () {
        var data = table.row( $(this).parents('tr') ).data();
        var txt = '{{asset('')}}delete-daftar/'+data.iddaftar;
        location = txt;
    } );
    
  });
</script>
@endsection



@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body " >
            <a href="{{asset('')}}add-daftar" type="button" class="btn btn-block btn-primary col-2 " >Tambah</a> <br>
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>ID Daftar</th>
                        <th>Tanggal Daftar</th>
                        <th>Semester</th>
                        <th>Tahun</th>
                        <th>NIM</th>
                        <th>Pendapatan</th>
                        <th>IPK</th>
                        <th>Saudara</th>
                        <th>Action</th>
                    </tr>
                </thead>
            <tbody>
            <td>
            
            </td>
            </tbody>
                <tfoot>
                    <tr>
                        <th>ID Daftar</th>
                        <th>Tanggal Daftar</th>
                        <th>Semester</th>
                        <th>Tahun</th>
                        <th>NIM</th>
                        <th>Pendapatan</th>
                        <th>IPK</th>
                        <th>Saudara</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
            </div>
        </div>
    </div>
    </div>
@endsection
