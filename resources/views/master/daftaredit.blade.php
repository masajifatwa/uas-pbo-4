@extends('layout.app')

@section('style')

@endsection

@section('script')

@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Data User</h3>
            </div>
            <div class="card-body">
                <form role="form" method="POST" action="{{asset('')}}update-daftar">
                   @csrf 
                    <div class="card-body">
                        <div class="form-group">
                            <label>iddaftar</label>
                            {{-- <input type="hidden" class="form-control" name="txtid" value="{{$daftar->id}}"> --}}
                            <input type="text" readonly class="form-control" name="iddaftar" value="{{$daftar->iddaftar}}" >
                        </div>
                        <div class="form-group">
                            <label>Tanggal Daftar</label>
                            <input type="date" class="form-control" name="tgldaftar" value="{{$daftar->tgldaftar}}" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <label>Semester</label>
                            <input type="text" class="form-control" name="semester" value="{{$daftar->semester}}" placeholder="Username">
                        </div>
                        <div class="form-group">
                            <label>tahun</label>
                            <input type="text" class="form-control" name="tahun" value="{{$daftar->tahun}}" placeholder="Password">
                        </div>
                        
                        <div class="form-group">
                            
                            <label>NIM</label>
                            <select name="nim" class="form-control">
                                
                                @foreach($mahasiswa as $m)
                                <option value="{{$m->nim}}" {{ ($m->nim == $m->nama) ? 'selected':''}} >{{$daftar->nim}}</option>
                                @endforeach
                            </select>

                            {{-- <input type="text" class="form-control" name="alamat" placeholder="Masukan Alamat"> --}}
                            
                        </div>
                        <div class="form-group">
                            <label>Pendapatan</label>
                            <input type="text" class="form-control" name="pendapatan" value="{{$daftar->pendapatan}}" placeholder="pendapatan">
                        </div>
                        <div class="form-group">
                            <label>IPK</label>
                            <input type="text" class="form-control" name="ipk" value="{{$daftar->ipk}}" placeholder="IPK">
                        </div>
                        <div class="form-group">
                            <label>Saudara</label>
                            <input type="text" class="form-control" name="saudara" value="{{$daftar->saudara}}" placeholder="Saudara">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Edit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection