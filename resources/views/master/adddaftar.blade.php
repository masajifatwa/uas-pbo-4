@extends('layout.app')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tambah Data Mahasiswa</h3>
            </div>
            <div class="card-body">
                <form role="form" method="POST" action="{{asset('')}}simpan-daftar">
			{{ csrf_field() }}
                   <div class="card-body">
                        <div class="form-group">
                            <div class="form-group">
                                <label>ID Daftar</label>
                                <input type="text" class="form-control" name="iddaftar" placeholder="ID Daftar">
                            </div>
                            <label>Tanggal Daftar</label>
                            <input type="date" class="form-control" name="tgldaftar" placeholder="Tanggal Daftar">
                        </div>
                        
                        <div class="form-group">
                            <label>Semester</label>
                            <input type="text" class="form-control" name="semester" placeholder="Masukan Semester">
                        </div>
                        <div class="form-group">
                            <label>Tahun</label>
                            <input type="text" class="form-control" name="tahun" placeholder="Masukan Tahun">
                        </div>
                        <div class="form-group">
                            
                            <label>NIM</label>
                            <select name="nim" class="form-control">
                                <option value="" disabled selected>Pilih NIM Mahasiswa</option>
                                @foreach($mahasiswa as $m)
                                <option value="{{$m->nim}}">{{$m->nim}}</option>
                                @endforeach
                            </select>
                            {{-- <input type="text" class="form-control" name="alamat" placeholder="Masukan Alamat"> --}}
                            
                        </div>
                        <div class="form-group">
                            <label>Pendapatan</label>
                            <input type="number" class="form-control" name="pendapatan" placeholder="Masukan Pendapatan">
                        </div>
                        <div class="form-group">
                            <label>IPK</label>
                            <input type="number" class="form-control" name="ipk" placeholder="Masukan IPK">
                        </div>
                        <div class="form-group">
                            <label>Saudara</label>
                            <input type="number" class="form-control" name="saudara" placeholder="Berapa Bersaudara">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection